#!/bin/sh

echo "Child $$: starting"

trap 'echo "Child $$: success"; exit 0' USR1
trap 'echo "Child $$: failure"; exit 1' USR2

while true; do
    sleep 1
done