#!/bin/sh

readonly USAGE="$0 only accepts one argument: the number of child processes to launch"

if [ ! $# -eq 1 ]; then echo $USAGE; exit 2; fi

for i in `seq 1 $1`; do
    eval "./child.sh &"
done

trap 'for job in `jobs -p`; do kill $job; done; exit 1' INT TERM

for job in `jobs -p`; do
    wait $job && { for job in `jobs -p`; do kill $job; done; exit 0; }
done
echo "All failed"
exit 1