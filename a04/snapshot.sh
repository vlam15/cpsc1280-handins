#!/bin/sh

snapDir="$PWD/.snapshot"
source="$PWD"

if [ ! -d "$snapDir" ]; then
    mkdir -v $snapDir
fi

cd $snapDir
if [ -d $snapDir/0  ]; then
    if [ -d $snapDir/9 ]; then
        cd $snapDir/9
        rm -vf $snapDir/9/*
        cd $snapDir
        rmdir -v $snapDir/9
    fi
    for dir in $( seq 8 -1 0 ); do
        if [ -d $snapDir/$dir ]; then
            mv -v $snapDir/$dir $snapDir/$(( $dir + 1 ))
        fi
    done
fi
mkdir -v "$snapDir/0"
cp -v $source/* $snapDir/0
cd $source