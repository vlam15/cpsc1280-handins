#!/bin/sh
if [ $# -eq 0 ]; then
    echo "$0 accepts at least one character as its argument" >&2
    exit 1
fi

while read line; do
    for char in "$@"; do
        printf "$( echo $line | tr -cd "$char" | wc -c ) "
    done
    echo 
done