#!/bin/sh
if [ $# -eq 0 ]; then
    echo "$0 accepts at least one glob as its argument" >&2
    exit 1
fi

while read line; do
    matched=true
     for glob in "$@"; do
        case "$line" in
            $glob) ;;
            *) matched=false;;
        esac
        if [ $matched = false ]; then
            break
        fi
     done
    if [ $matched = true ]; then
        echo "$line"
    fi
done