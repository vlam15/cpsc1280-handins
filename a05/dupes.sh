#!/bin/sh

status=0

hasDupes() {
    if [ ! -f $1 ]; then return 1; fi
    original=$1
    shift
    # empty files
    if [ ! -s $original ]; then
        for file in $@; do
            if [ ! -f $file ]; then continue; fi
            if [ ! -s $file ]; then
                return 0
            fi
        done
    # non-empty files
    else 
        for file in $@; do
            if [ ! -f $file ]; then continue; fi
            if [ -s $file ] && [ `cat $original` == `cat $file` ]; then
                return 0
            fi
        done
    fi
    return 1
}
# if no argument is passed, recursively call the function to check the entire directory 
if [ $# -eq 0 ]; then
    ./$0 *
else 
    for file in $@; do
        if [ ! -f $file ]; then
            echo ":$file: is not a file"
            status=2
        else 
            printf $file
            for next in $*; do
                if [ $file != $next ] && `hasDupes $file $next`; then
                    if [ $status -lt 1 ]; then
                        status=1
                    fi
                    printf ":$next"
                fi
            done
            echo
        fi
    done
    exit $status
fi
